import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {Accounts} from 'meteor/accounts-base';


class Signup extends Component {

    constructor() {
        super();
        this.state = {
            error: ''
        };

    }


    onSubmit(e) {
        e.preventDefault();

        let email = this.refs.email.value.trim();
        let password = this.refs.password.value.trim();

        if (password.length < 9) {
            return this.setState({error: 'Password must be more than 9 characters long'});
        }

        Accounts.createUser({
            email,
            password
        }, (error) => {

            if (error) {

                this.setState({
                    error: error.reason
                });

            } else {
                this.setState({
                    error: ''
                })
                this.props.history.replace('/dashboard');

            }


        });


    }

    render() {
        return (

            <div className="boxed-view">
                <div className="boxed-view__box">
                    <h1>Join</h1>

                    {this.state.error ? <p>{this.state.error}</p> : undefined }

                    <form onSubmit={this.onSubmit.bind(this)} noValidate className="boxed-view__form">
                        <input type="email" name="email" placeholder="Email" ref="email"/>
                        <input type="password" name="password" placeholder="Password" ref="password"/>
                        <button className="button">Create Account</button>
                    </form>

                    <Link to="/">Already have an account? </Link>
                </div>
            </div>
        );

    }
}

export default Signup;