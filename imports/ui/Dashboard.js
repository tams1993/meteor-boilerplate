import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import PrivateHeader from './PrivateHeader';

export default (props) => {


    if (!Meteor.userId()) {
        return (props.history.push('/'));
    } else {
        return (

            <div>
                <PrivateHeader title="Dashboard" history={props.history}/>
                <div className="page-content">
                   Page Content
                </div>
            </div>
        );
    }


}
